 export const Combos: any[] = [
		   {
		   specialtyId: "5bdbc0cb98ababd452be1170",
		   quantity: 1, 
		   data: {
		   title: "2-Meal Combo Plan",
		   price: 8.99,
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Pinakbet",
		   description: "Vegetable stew cooked with ginger, garlic, onion, tomato, okra, string beans, squash, and eggplant"
		   }
		  },
		   {
		   id: "5bdbc0cb98ababd452be1171",
		   quantity: 1, 
		   data: {
		   title: "2-Meal Combo Plan",
		   price: 8.99,
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Pork Adobo",
		   description: "Pork cooked in garlic, onions, bay leaves, soy sauce, and vinegar"
		   }
		   },
		    {
		   id: "5bdbc0cb98ababd452be1172",
		   quantity: 1, 
		   data:{
			title: "2-Meal Combo Plan",
			price: 8.99,
			image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		    combo: "",
		    name: "Chicken Kaldereta",
		    description: "Chicken cooked in garlic, onions, bay leaves, vinegar, tomato sauce, bell peppers, potatoes, and carrots"
		   }
		   },
		   {
			id: "5bdbc0cb98ababd452be1173",
			quantity: 1,
			data: {
			title: "2-Meal Combo Plan",
			price: 7.99,
			image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
			combo: "",
			name: "Tinola",
			description: "Chicken cooked with ginger, garlic, onion, green papaya and spinach"
			}
			},
			{
			id: "5bdbc0cb98ababd452be1174",
			quantity: 1, 
			data: {
			title: "2-Meal Combo Plan",
			price: 7.99,
			image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
			combo: "",
			name: "Lechon Kawali",
			description: "Crispy roast pork"
			}
			},
			{ 
			id: "5bdbc0cb98ababd452be1175",
			quantity: 1,
			data: {
			title: "2-Meal Combo Plan",
			price: 7.99,
			image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
			combo: "",
			name: "Chopsuey",
			description: "Vegetable saute with garlic, onions, bell peppers, baby corn, snap peas"	
			}
			},
			{
				title: "2-Meal Combo Plan",
				id: "5bdbc0cb98ababd452be1176",
				price: 7.99,
				quantity: 1, 
				data: {
				title: "2-Meal Combo Plan",
			    price: 7.99,
				image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "",
				name: "Chicken Inasal",
				description: "Roast chicken marinated in lemon, orange, lime, pepper, coconut vinegar and annato"	
				}
				},
				{
				id: "5bdbc0cb98ababd452be1177",
				quantity: 1, 
				data: {
				title: "2-Meal Combo Plan",
				price: 7.99,	
				image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "",
				name: "Ginisang Ampalaya",
				description: "Bittermelon sauteed in garlic, onion, tomatoes and eggs"
				}
				},
				{ 
				id: "5bdbc0cb98ababd452be1178",
				quantity: 1,
				data:{
				title: "2-Meal Combo Plan",
				price: 7.99,
                image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "",
				name: "Pork Sinigang",
				description: "Pork stew with onion, tomatoes, radish, long green beans, eggplant and okra"
				}
				},
				{
				id: "5bdbc0cb98ababd452be1179",
				quantity: 1, 
				data: {
				title: "2-Meal Combo Plan",
				price: 7.99,
                image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "",
				name: "Chicken Adobo",
				description: "Chicken cooked in garlic, onions, bay leaves, soy sauce, and vinegar"
				}
				},
				{
				id: "5bdbc0cb98ababd452be1180",
				quantity: 1, 
				data: {
				title: "2-Meal Combo Plan",
				price: 7.99,
				image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "",
				name: "Pancit Bihon",
				description: "Rice noodles cooked with soy sauce, garlic, onion, pork, chicken, snowpeas, carrots, cabbage, celery leaves"
				}
				},
			    { 
				id: "5bdbc0cb98ababd452be1181",
				quantity: 1,
				data: {
				title: "2-Meal Combo Plan",
				price: 7.99,
				image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "",
				name: "Beef Pochero",
				description: "Beef stew cooked with garlic, onion, tomato, cabbage, green beans, potato, garbanzos"
				}
				},
				{
				id: "5bdbc0cb98ababd452be1182",
				quantity: 1, 
				data: {
				title: "2-Meal Combo Plan",
				price: 7.99,
                image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "",
				name: "Sinampalukang Manok",
				description: "Chicken cooked with ginger, garlic, onion, green papaya, tamarind leaves"
				}
				},
				{
				id: "5bdbc0cb98ababd452be1183",
				quantity: 1,
				data: {
				title: "2-Meal Combo Plan",
				price: 7.99,
				image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "",
				name: "Pritong Bangus",
				description: "Fried milkfish"
				} 
	          },
	          { 
			   id: "5bdbc0cb98ababd452be1184",
			   quantity: 1,
			   data: {
				title: "2-Meal Combo Plan",
				price: 7.99,
				image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "",
				name: "Ginisang Sayote",
				description: "Chayote saute with garlic, onions, tomatoes and pork"
			   }
			   },
			   {
				id: "5bdbc0cb98ababd452be1185",
				quantity: 1, 
				data: {
				title: "2-Meal Combo Plan",
				price: 7.99,
				image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "",
				name: "Pancit Palabok",
				description: "Rice noodles cooked in pork broth and topped with pork, tofu, shrimp, fried pork skin, fried garlic, green onions and boiled eggs"
				}
				},
			    {
				id: "5bdbc0cb98ababd452be1186",
				quantity: 1, 
				data: {
				title: "2-Meal Combo Plan",
				price: 7.99,
				image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "",
				name: "Lumpiang Shanghai",
				description: "Spring rolls filled with a mix of ground pork and ground beef, carrots, onions"
				}
				},
				{ 
				id: "5bdbc0cb98ababd452be1187",
				quantity: 1,
				data: {
				title: "2-Meal Combo Plan",
				price: 7.99,
				image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "",
				name: "Bichuelas",
				description: "Beef stew with navy beans and cabbage"
				}
				},
				{ 
				id: "5bdbc0cb98ababd452be1191",
				quantity: 1,
				data: {
				title: "3-Meal Combo Plan",
				price: 7.99,
                image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "1",
				name: "Bistek Tagalog",
				description: "Beef sirloin sauteed in garlic, onions, soy sauce"
				}
				},
				{ 
				id: "5bdbc0cb98ababd452be1195",
				quantity: 1,
				data: {
				title: "3-Meal Combo Plan",
				price: 7.99,
				image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "1",
				name: "Beef Nilaga",
				description: "Beef stew with bok choy, carrots, and potatoes"	
				}
				},
			   {
				id: "5bdbc0cb98ababd452be1202",
				quantity: 1, 
				data: {
				title: "3-Meal Combo Plan",
				price: 7.99,
				image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "",
				name: "Beef Tapa",
				description: "Chicken cooked with ginger, garlic, onion, green papaya and spinach"
				}
				},
				{
				id: "5bdbc0cb98ababd452be1207",
				quantity: 1, 
				data: {
				title: "3-Meal Combo Plan",
				price: 7.99,
				image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "1",
				name: "Misua",
				description: "Japanese somen noodle soup with ground pork"	
				}
				},
				{ 
				id: "5bdbc0cb98ababd452be1208",
				quantity: 1,
				data: {
				title: "3-Meal Combo Plan",
				price: 7.99,
				image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "1",
				name: "Bicol Express",
				description: "Pork cooked with garlic, onion, green chili peppers, shrimp paste and coconut cream"
				}
				},

		]

