import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { authService } from '../../../providers/auth.service';
import { ordersService } from '../../../providers/order.service';
import { cartService } from '../../../providers/cart.service';
import { paymentService } from '../../../providers/payment.service';
import {Router} from '@angular/router'; 
import { Combos } from './mealplandata';
import { last } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class Cart implements OnInit {

user: any = {};
updateForm: any;

addMeals = Combos;
selectedMeal: any;

tax: number;
orders: any = []
meals: any = []
subtotal: number = 0;
total: number = 0;

deliverydate: string;
firstname;
lastname;
email;
message: string;

cardnumber;
month;
year;
cvc;
billstreet;
billcity;
billstate;
billzipcode;

checkboxValue: boolean;



constructor(private authService : authService, public router : Router, public paymentService : paymentService,
	    public orderService : ordersService, public cartService : cartService, public zone: NgZone ){
        this.updateForm = new FormGroup({
          firstname: new FormControl(''),
          lastname:  new FormControl(''),
          street:  new FormControl(''),
          city:  new FormControl(''),
          state:  new FormControl(''),
          zipcode:  new FormControl(''),
          phone:  new FormControl(''),
          email: new FormControl(''),
        })
      }

  ngOnInit(){

       if(this.authService.loggedIn()) {
			 this.orderService.getAllOrders()
        .subscribe((res) => {
         console.log(res)
			   this.orders = res[0];
         this.deliverydate = this.orders.data.deliveryDate;
         this.message = this.orders.data.message;
			   this.subTotal();
         })
         this.authService.getMe().subscribe(res => {
          console.log(res);
          this.user = res;
          this.email = res.email;
          this.firstname = res.firstName;
          this.lastname = res.lastName;
          this.updateForm.patchValue({
            firstname: res.firstName,
            lastname: res.lastName,
            street: res.data.street,
            city: res.data.city,
            state: res.data.state,
            zipcode: res.data.zipcode,
            phone: res.data.phone,
            email: res.email,
          });
        });
			   } else {
           this.router.navigate(['']);
         }
        }

 changeAddress(){
   if(this.checkboxValue){
    this.billstreet = this.updateForm.get('street').value;
    this.billcity = this.updateForm.get('city').value;
    this.billstate = this.updateForm.get('state').value;
    this.billzipcode = this.updateForm.get('zipcode').value;
   } else {
     this.billstreet = '';
     this.billcity = '';
     this.billstate = '';
     this.billzipcode = '';
   }
 }
 
 subTotal(){
    this.subtotal = 0;
    for(let i=0;i<this.orders.specialties.length;i++){
      this.subtotal += (this.orders.specialties[i].data.price * this.orders.specialties[i].quantity);
    }
  this.calculateTax();
  console.log(this.orders)
  this.orderService.modifyOrder(this.orders, this.orders._id)
  .subscribe((data)=>
   console.log(data)
   );
  }

  add(count, i){ 
    this.orders.specialties[i].quantity = count + 1;
    console.log(this.orders)
    this.orderService.modifyOrder(this.orders, this.orders._id)
    .subscribe((data)=>
     console.log(data)
     );
	this.subTotal();
  }

  del(count, i){ 
    if(count <= 0){
     this.orders.specialties[i].quantity = 0;  
     console.log(this.orders)
    } else {
   this.orders.specialties[i].quantity = count - 1;
   console.log(this.orders)
   this.orderService.modifyOrder(this.orders, this.orders._id)
   .subscribe((data)=>
    console.log(data)
    );
   this.subTotal();
  }
  }
  
  calculateTax() {
    const salesTax = .0725;
    this.tax = (this.subtotal) * (salesTax);
    this.totalPrice();
    this.orderService.modifyOrder(this.orders, this.orders._id)
    .subscribe((data)=>
     console.log(data)
     );
}

 totalPrice(){
 this.total = (this.subtotal + this.tax)
 this.orderService.modifyOrder(this.orders, this.orders._id)
 .subscribe((data)=>
  console.log(data)
  );
 }

 deleteOrder(id, index){
  this.orders.specialties.splice(index, 1);
  console.log(this.orders.specialties)
  this.subTotal();
  this.orderService.modifyOrder(this.orders, this.orders._id)
  .subscribe((data)=>
   console.log(data)
   );
}

addMeal(val){
this.orders.specialties.push(this.selectedMeal)
console.log(this.selectedMeal)
console.log(this.orders.specialties)
this.subTotal();

const mealAdded = Object.values(this.orders.specialties.reduce((add, m) => {
  let meal = m.data.name;
  if(!add[meal]) add[meal] = {...m, quantity: m.quantity}
  else add[meal].quantity += 1;
  return add;
}, {}))

console.log(mealAdded)
this.orders.specialties = mealAdded; 
console.log(this.orders)
this.subTotal();
this.selectedMeal = undefined;
val.target.value = undefined;
}

delivery(){
  this.orders.data.deliveryDate = this.deliverydate;
  this.orderService.modifyOrder(this.orders, this.orders._id)
  .subscribe((data)=> {
   console.log(data)
   })
  }

  leaveMsg(){
  this.orders.data.message = this.message;
  this.orderService.modifyOrder(this.orders, this.orders._id)
  .subscribe((data)=> {
   console.log(data)
   })
  }

  updateUser() {
    let email = this.updateForm.get('email').value;
    let firstname = this.updateForm.get('firstname').value;
    let lastname =  this.updateForm.get('lastname').value;
    
    let street = this.updateForm.get('street').value;
    let city = this.updateForm.get('city').value;
    let state = this.updateForm.get('state').value;;
    let zipcode = this.updateForm.get('zipcode').value;;
    let phone = this.updateForm.get('phone').value;;
    
    let account = {
      firstName: firstname,
      lastName: lastname,
      email: email,
          data: {
           "phone": phone,
           "street" : street,
           "city": city,
           "state": state,
           "zipcode": zipcode
            }
                   }
      this.authService.updateMe(account).subscribe(
        response => {
          console.log(response);
          alert("Your details are updated!");
        },
        error =>
          alert(
            "Your details are not submitted. Please try again" + "error:" + error
          )
      );
    }

    getToken() {
     const self = this;
       //self.message = 'Sending details...';
     alert('Sending details');
     //self.showSelected =  true;
       (<any>window).Stripe.card.createToken({
         number: self.cardnumber,
         exp_month: self.month,
         exp_year: self.year,
         cvc: self.cvc,
         name: self.firstname + self.lastname,
         address_line1: self.billstreet,
         address_city: self.billcity,
         address_state: self.billstate,
         address_zip: self.billzipcode
       }, (status: number, response: any) => {
   
           this.zone.run(() => {
           if (status === 200) {
           alert('Success! Your card has been added');
           const sourceId = response.id;
              self.paymentService.addCard(sourceId)
             .take(1).subscribe((data) => {
               if(!data.id){
                 alert(response.error.message)
               } else {
               console.log('get existing card')
        //  self.paymentService.getCards().subscribe(data => {
        //      self.ccList = data;
   
        //       });
            }
   
         });
         this.processOrder();
       }
     });
     });
     }

     processOrder(){

     }

}