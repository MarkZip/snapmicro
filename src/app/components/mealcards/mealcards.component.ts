import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-mealcards',
  templateUrl: './mealcards.component.html',
  styleUrls: ['./mealcards.component.css']
})
export class MealCards implements OnInit {

  @Input() list;
  fulldetails : any[];

  constructor() { }

  ngOnInit() {

    this.list.subscribe(
      det => {
        console.log(det);
        this.fulldetails = det; 
      },
      error => {
        console.log(error);
      } 
    )
  }

}