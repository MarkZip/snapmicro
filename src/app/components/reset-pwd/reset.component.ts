import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute} from '@angular/router';
import { authService } from '../../../providers/auth.service';


@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetPassword implements OnInit {
  
    confirmpwd: FormControl;
    newpwd: FormControl;
   
    newpassword;
    confirmpassword;
    token:any;
    email:any;

  constructor(public authService: authService, public router : Router, private route: ActivatedRoute) {
    this.newpwd = new FormControl('', [Validators.minLength(6), Validators.required]);
    this.confirmpwd = new FormControl('', [Validators.minLength(6), Validators.required]);
    this.token = this.route.snapshot.paramMap.get('id');
    console.log(this.token);
   }

  ngOnInit() {}

  resetPassword(){
    this.authService.changePassword(this.token, this.newpassword)
      .subscribe((data) => {
          console.log(data + 'password changed')
          alert('Password successfully changed!')
          this.router.navigate(['/home']);
      }, (error) => {
          console.log(error)

      });
}

}