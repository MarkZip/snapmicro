import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { authService } from '../../../providers/auth.service';
import { ordersService } from '../../../providers/order.service';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-activate',
  templateUrl: './activate.component.html',
  styleUrls: ['./activate.component.css']
})
export class ActivateAccount implements OnInit {

 token:string;
 isVisible: boolean = false;
 fulldetails: any = {};
 orders = [];

 @Input() list : Subject<any>;
 @Input() creds: any;
 @Output() loginAccount = new EventEmitter();
 @Output() myOrder = new EventEmitter();

 public processOrder = {
  name: '',
  sourceId:'',
  data:{
   firstname:'',
   lastname:'',
   street:'',
   city: '',
   state:'',
   zipcode:'',
   phone: '',
   email: '',
   deliveryDate: '',
   message: '',
  },
  specialties:[],
  stripeCustomerId: '' ,
  notes: '',

}

constructor(public authService: authService,  public orderService : ordersService, public router : Router, private route: ActivatedRoute) {
  // this.token = this.route.snapshot.paramMap.get('id');
 
}
  ngOnInit() {
    this.list.subscribe(
      det => {
        console.log(det);
        this.fulldetails = det; 
      },
      error => {
        console.log(error);
      } 
    )
  }

  activate(){
    console.log(this.fulldetails)
    console.log(this.creds)
    console.log(this.token);
    this.authService.activate(this.token)
        .subscribe(
            res =>{
                console.log(res)
                this.loginUser();
            },
            err =>{
                console.log(err)
                alert("There is an error activating your account. Please call us at 1-800-OHANAMEALS")
                this.loginUser();
            }
        )
  }

  loginUser() {
  let email = this.creds.email;
  let password = this.creds.password;
  this.authService.login(email, password)
   .subscribe(
   (response) => {
   console.log('logged in!')
   if(this.list) {
   this.addToCart();
   } else {
   this.myOrders();
  }
  },
  (error) => alert("Login and/or password not recognized")
  );
  }

  myOrders(){
    this.myOrder.emit()
  }

  // loginNow(){
  //   this.loginAccount.emit();
  // }

  addToCart() {
    this.processOrder.name = this.fulldetails.name; 
      this.processOrder.specialties = this.fulldetails.product_combos.map(combo =>({
    specialtyId: combo.id,
    quantity: combo.quantity,
      data: {
      price: combo.price,
      name: combo.name,
      image: combo.image,
      description: combo.description,
      title: combo.title,
      }
    })); 
       this.orderService.getAllOrders()
       .subscribe(res => {
       this.orders = res;
        if( this.orders.length === 0 || this.orders[0].status !== 1){
          this.orderService.addOrder(this.processOrder)
         .subscribe(data => console.log(data));	
         console.log('add new')
         this.router.navigate(['/shopping-cart']);
       } else {
        console.log('modify')
        const orderID = this.orders[0].specialties.map(order => order.specialtyId)
        console.log(orderID)
        const detailsID = this.fulldetails.product_combos.map(detail => detail.id)
        console.log(detailsID)
            if( detailsID.toString() === orderID.toString())
             {  
         for (var i = 0; i < this.orders[0].specialties.length; i++) {
             this.orders[0].specialties[i].quantity += 1;
         }
         console.log('added to existing order')
         console.log(this.orders[0])
          this.orderService.modifyOrder(this.orders[0], this.orders[0]._id)
         .subscribe(data => {
         console.log(data)
         this.router.navigate(['/shopping-cart']);
         });
         this.router.navigate(['/shopping-cart']);	
             } else {
              //  this.orders[0].specialties.push(this.fulldetails.product_combos.map(combo =>({
              //     specialtyId: combo.id,
              //     quantity: combo.quantity,
              //     data: {
              //     price: combo.price,
              //     name: combo.name,
              //     image: combo.image,
              //     description: combo.description,
              //     title: combo.title,
              //     }
              //     })))
  
                  this.fulldetails.product_combos.map(combo => {
                    this.orders[0].specialties.push({
                      specialtyId: combo.id,
                      quantity: combo.quantity,
                       data: {
                       price: combo.price,
                       name: combo.name,
                       image: combo.image,
                       description: combo.description,
                       title: combo.title,
                    }})
                  })
  
  
         console.log('added new combo')
         console.log(this.orders[0])
         this.orderService.modifyOrder(this.orders[0], this.orders[0]._id)
         .subscribe(data => {
         console.log(data)
         this.router.navigate(['/shopping-cart']);
         });
         //this.router.navigate(['/shopping-cart']);
         }  
        }
       })
          }
  }

