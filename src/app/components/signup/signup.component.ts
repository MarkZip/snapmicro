import { Component, OnInit, Input, Output, EventEmitter, OnChanges} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Router} from '@angular/router';
import { authService } from '../../../providers/auth.service';
import { ordersService } from '../../../providers/order.service';
import { paymentService } from '../../../providers/payment.service'
import { Subject } from 'rxjs';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignUp implements OnInit, OnChanges {

  public loginForm: any;
  public signUpForm: any;
  @Input() menuItem : Subject<any>;
  @Input() list : Subject<any>;
  @Input() clearform: boolean = false;
  @Output() resetPwd = new EventEmitter();
  @Output() myOrder = new EventEmitter();
  @Output() create = new EventEmitter();
  fulldetails: any = {};
  item: any;
  user: any = {};
  orders = [];
  errormsg;
  registrationError;
  registrationSuccess;
  
  public OrderSpecialty = {
  specialtyId:'',
  quantity:'',
  data: {
  price:'',
  name:'',
  image:'',
  description:'',
  title:'',
  } 
  }
 
  public processOrder = {
    name: '',
    sourceId:'',
    data:{
     firstname:'',
     lastname:'',
     street:'',
     city: '',
     state:'',
     zipcode:'',
     phone: '',
     email: '',
     deliveryDate: '',
     message: '',
    },
    specialties:[],
    stripeCustomerId: '' ,
    notes: '',
  
  }
  
  constructor(public authService: authService, public paymentService : paymentService,
	    public orderService : ordersService, public router : Router) {

    this.loginForm = new FormGroup({
      email: new FormControl(''),
      password:  new FormControl ('')  
    });
	


   this.signUpForm = new FormGroup({
      firstname: new FormControl('', [Validators.minLength(1), Validators.required]),
	    lastname:  new FormControl('', [Validators.minLength(1), Validators.required]),
      email: new FormControl('', [Validators.email, Validators.required]),
      password: new FormControl('', [Validators.minLength(6), Validators.required]),
    })

  }
  
  ngOnInit(){

    this.menuItem.subscribe(
      det => {
        console.log(det);
        this.item = det; 
      },
      error => {
        console.log(error);
      } 
    )
    
    this.list.subscribe(
      det => {
        console.log(det);
        this.fulldetails = det; 
      },
      error => {
        console.log(error);
      } 
    )
  }

  ngOnChanges() {
   if(this.clearform === true){
     this.loginForm.reset();
     this.signUpForm.reset();
     this.errormsg = '';
   }
  }
  
   getMe(){
    this.authService.getMe().subscribe(res => {
        console.log(res);
	    this.user = res;
		this.addToCart();
		if(!this.user.stripeCustomerId){
        this.paymentService.createCustomer()
		.subscribe(data => { 
		 console.log(data); 
		 //this.addToCart();
		});
      }  
      
      this.processOrder.stripeCustomerId = this.user.stripeCustomerId;
      this.processOrder.data.street = this.user.data.street ? this.user.data.street : '';
      this.processOrder.data.city = this.user.data.city ? this.user.data.city : '' ;
      this.processOrder.data.state = this.user.data.state ? this.user.data.state : '';
      this.processOrder.data.zipcode = this.user.data.zipcode ? this.user.data.zipcode : '';
      this.processOrder.data.phone = this.user.data.phone ? this.user.data.phone : '';
      this.processOrder.data.firstname = this.user.firstName ? this.user.firstName: '' ;
      this.processOrder.data.lastname = this.user.lastName ? this.user.lastName: '' ;
      this.processOrder.data.email = this.user.email ? this.user.email: '' ;
	       
			
			});
  }

  	
  signUpUser() {
    this.create.emit();
    //   let email = this.signUpForm.get('email').value;
    //   let password = this.signUpForm.get('password').value;
	  // let firstname = this.signUpForm.get('firstname').value;
	  // let lastname =  this.signUpForm.get('lastname').value;
	  
	  // let street = '';
	  // let city = '';
	  // let state = '';
	  // let zipcode = '';
	  // let phone = '';
	  
	  // let contact = {
	  //        data: {
		// 	       "phone": phone,
		// 	       "street" : street,
		// 		     "city": city,
		// 		     "state": state,
		// 		     "zipcode": zipcode
		// 		         }
    //                }
    //   this.authService.signup(email, password, firstname, lastname)
	  //   .subscribe(
	  //   (response) => {
	  //   this.registrationSuccess = 'Thank you for signing up!'
    //   this.getMe();
    //   if(this.list) {
    //     this.addToCart();
    //   } else {
    //    this.myOrders();
    //   }
	  //   },
	  
	  //   (error) => {
    //     this.registrationError ='Your details are not submitted.'
    //   }
	  //  );
	  
  }


  public loginUser() {
        let email = this.loginForm.get('email').value;
        let password = this.loginForm.get('password').value;
      this.authService.login(email, password)
	  .subscribe(
	  (response) => {
      console.log('logged in!')
       this.getMe();
    if(this.list && this.item !== 'account' && this.item !== 'orders' ) {
      this.addToCart();
    } else {
     this.myOrders();
    }
	  },
	  (error) => this.errormsg = "Login and/or password not recognized"
	  );
    }

public LoginFB() {
  this.authService.loginFacebook(function() {
  alert('FBBBB!!')
})
}

public LoginGoogle() {
  this.authService.loginGoogle(function() {
  alert('GOOGLE')
})
}

resetPassword(){
  this.resetPwd.emit( this.router.navigate(['/my-account/password']))
}

myOrders(){
  this.myOrder.emit(this.item)
}
  
  addToCart() {
	this.processOrder.name = this.fulldetails.name; 
  this.processOrder.specialties = this.fulldetails.product_combos.map(combo =>({
	specialtyId: combo.id,
	quantity: combo.quantity,
    data: {
    price: combo.price,
    name: combo.name,
    image: combo.image,
    description: combo.description,
    title: combo.title,
    }
	})); 
		 this.orderService.getAllOrders()
		 .subscribe(res => {
		 this.orders = res;
		  if( this.orders.length === 0 || this.orders[0].status !== 1){
		 	 this.orderService.addOrder(this.processOrder)
			 .subscribe(data => console.log(data));	
			 console.log('add new')
			 this.router.navigate(['/shopping-cart']);
		 } else {
		  console.log('modify')
		  const orderID = this.orders[0].specialties.map(order => order.specialtyId)
		  console.log(orderID)
		  const detailsID = this.fulldetails.product_combos.map(detail => detail.id)
		  console.log(detailsID)
          if( detailsID.toString() === orderID.toString())
           {  
		   for (var i = 0; i < this.orders[0].specialties.length; i++) {
           this.orders[0].specialties[i].quantity += 1;
		   }
		   console.log('added to existing order')
		   console.log(this.orders[0])
		    this.orderService.modifyOrder(this.orders[0], this.orders[0]._id)
			 .subscribe(data => {
			 console.log(data)
			 this.router.navigate(['/shopping-cart']);
       });
       this.router.navigate(['/shopping-cart']);	
           } else {
            //  this.orders[0].specialties.push(this.fulldetails.product_combos.map(combo =>({
            //     specialtyId: combo.id,
            //     quantity: combo.quantity,
            //     data: {
            //     price: combo.price,
            //     name: combo.name,
            //     image: combo.image,
            //     description: combo.description,
            //     title: combo.title,
            //     }
            //     })))

                this.fulldetails.product_combos.map(combo => {
                  this.orders[0].specialties.push({
                    specialtyId: combo.id,
                    quantity: combo.quantity,
                     data: {
                     price: combo.price,
                     name: combo.name,
                     image: combo.image,
                     description: combo.description,
                     title: combo.title,
                  }})
                })


			 console.log('added new combo')
			 console.log(this.orders[0])
			 this.orderService.modifyOrder(this.orders[0], this.orders[0]._id)
			 .subscribe(data => {
			 console.log(data)
			 this.router.navigate(['/shopping-cart']);
       });
       this.router.navigate(['/shopping-cart']);
		   }  
		  }
		 })
        }
  }