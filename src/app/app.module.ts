import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ErrorHandler, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AccountComponent } from './my-account/account.component';
import { MealPlans } from './mealplans/mealplans.component';
import { MealCards } from './components/mealcards/mealcards.component';
import { Cart } from './components/shopping-cart/cart.component';
import { SignUp } from './components/signup/signup.component';
import { CreateAccount } from './components/createAccount/createAccount.component';
import { ResetPassword } from './components/reset-pwd/reset.component';
import { ActivateAccount } from './components/activate/activate.component';

import {authService} from '../providers/auth.service';
import {ordersService} from '../providers/order.service';
import {paymentService} from '../providers/payment.service';
import {cartService} from '../providers/cart.service';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

const appRoutes: Routes =[
{ path: '', component: HomeComponent },
{ path: 'home', component: HomeComponent },
{ path: 'activate', component: ActivateAccount },
{ path: 'newpassword/:id', component: ResetPassword },
{ path: 'my-account', component: AccountComponent},
{ path: 'my-account/password', component: AccountComponent},
{ path: 'my-account/orders', component: AccountComponent},
{ path: 'mealplans', component: MealPlans},
{ path: 'shopping-cart', component: Cart },
{ path: 'mealcards', component: MealCards },
{ path: 'signup', component: SignUp },
{ path: 'create', component: CreateAccount },
];


@NgModule({
  declarations: [
   AppComponent,
   HomeComponent,
   AccountComponent,
   ActivateAccount,
   ResetPassword,
   MealPlans,
   MealCards,
   SignUp,
   CreateAccount,
   Cart,
  ],
  imports: [
    BrowserModule,
	BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
	FormsModule,
	ReactiveFormsModule,
    HttpModule,	
	RouterModule.forRoot(appRoutes,{useHash: true})
  ],
  
  schemas: [ NO_ERRORS_SCHEMA ],
  
  
  providers: [
  authService,
  ordersService,
  paymentService,
  cartService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
