import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { authService } from '../../providers/auth.service';
import { ordersService } from '../../providers/order.service';
import { ModalDirective } from 'angular-bootstrap-md';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {
  
  menuItem: Subject<any> = new Subject;

  @ViewChild('signupModal') signupModal: ModalDirective;
  @ViewChild('createAccount') createAccount: ModalDirective;
  @ViewChild('activateAccount') activateAccount: ModalDirective;

  hasItems: boolean;
  orders;
  clearForm: boolean;
  credentials: any;
  
  constructor(
    private authService : authService, 
    public orderService : ordersService, 
    public router : Router) { }


  ngOnInit() {
    this.orderService.getAllOrders()
         .subscribe((res) => {
         this.orders = res[0].specialties;
         if(this.orders.length > 0 && this.authService.loggedIn() ){
          console.log(this.orders)
          this.hasItems = true;
         }
         })
  }

   ngAfterViewInit() {
    if(!this.authService.loggedIn() && this.router.url === '/home'){
      this.signupModal.show();
    } else {
      this.router.navigate(['']);
    }
  }

  myAccount(){
    this.menuItem.next('account');
    if(this.authService.loggedIn()){
      this.router.navigate(['/my-account']);
  } else {
    this.signupModal.show();
    this.clearForm = false;
  }
}
  
  myOrders(){
    this.menuItem.next('orders');
    if(this.authService.loggedIn()){
      if(this.orders.length > 0) {
      this.router.navigate(['/my-account/orders']);
      } else {
        alert('You have no orders yet.')
      }
  } else {
    this.signupModal.show();
    this.clearForm = false;
  }
}

closeModal(){
  this.signupModal.hide();
  this.clearForm = true;
}

closeCreateModal(){
  this.createAccount.hide();
  this.clearForm = true;
}

closeActivateModal(){
  this.activateAccount.hide();
}

resetPassword(){
  this.closeModal();
}

myOrder(event){
  console.log(event)
  console.log(this.orders)
  if(event === 'account'){
  this.router.navigate(['/my-account']);
  } else {
  this.router.navigate(['/my-account/orders']);
  }
  this.closeModal();
  this.closeActivateModal();
}

loginAccount(){
  this.closeCreateModal();
  this.closeActivateModal();
  this.signupModal.show();
}

create(){
  this.closeModal();
  this.createAccount.show();
}

activate(event){
  this.credentials = event;
  console.log(this.credentials)
  this.closeCreateModal();
  this.activateAccount.show();
}
}
