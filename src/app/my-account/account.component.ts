import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Router} from '@angular/router';
import { authService } from '../../providers/auth.service';
import { ordersService } from '../../providers/order.service';

@Component({
  selector: 'my-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  
  changeEmail:boolean;
  changePassword:boolean;
  changeProfile: boolean;
  public signUpForm: any;

  email;
  orders = [];
  hasItems: boolean;

  constructor(private authService : authService, public router : Router, public orderService : ordersService) { 
    
    this.showChangeProfile();

    this.signUpForm = new FormGroup({
      firstname: new FormControl('', [Validators.minLength(1), Validators.required]),
      lastname:  new FormControl('', [Validators.minLength(1), Validators.required]),
      street:  new FormControl('', [Validators.required]),
      city:  new FormControl('', [Validators.required]),
      state:  new FormControl('', [Validators.required]),
      zipcode:  new FormControl('', [Validators.required]),
      phone:  new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.email, Validators.required]),
    })
  }

  ngOnInit() {
    if(this.router.url === '/my-account/password'){
      this.showChangePassword();
    } else if (this.router.url === '/my-account/orders') {
      this.showChangeEmail();
    }
    this.authService.getMe().subscribe(
      res => {
        console.log(res);
        this.email = res.email;
        this.signUpForm.patchValue({
          firstname: res.firstName,
          lastname: res.lastName,
          street: res.data.street,
          city: res.data.city,
          state: res.data.state,
          zipcode: res.data.zipcode,
          phone: res.data.phone,
          email: res.email,
        });
      },
      error => console.log(error)
    );

    this.orderService.getAllOrders()
         .subscribe((res) => {
         this.orders = res[0].specialties;
         if(this.orders.length > 0 && this.authService.loggedIn() ){
          console.log(this.orders)
          this.hasItems = true;
         }
         })
  }

  showChangeEmail() {
    this.changeEmail = true;
    this.changePassword = false;
    this.changeProfile = false;
  }

  showChangePassword() {
    this.changeEmail = false;
    this.changePassword = true;
    this.changeProfile = false;
  }
  showChangeProfile() {
    this.changeEmail = false;
    this.changePassword = false;
    this.changeProfile = true;
  }

  updateUser() {
  let email = this.signUpForm.get('email').value;
  let firstname = this.signUpForm.get('firstname').value;
  let lastname =  this.signUpForm.get('lastname').value;
  
  let street = this.signUpForm.get('street').value;
  let city = this.signUpForm.get('city').value;
  let state = this.signUpForm.get('state').value;;
  let zipcode = this.signUpForm.get('zipcode').value;;
  let phone = this.signUpForm.get('phone').value;;
  
  let account = {
    firstName: firstname,
    lastName: lastname,
    email: email,
        data: {
         "phone": phone,
         "street" : street,
         "city": city,
         "state": state,
         "zipcode": zipcode
          }
                 }
    this.authService.updateMe(account).subscribe(
      response => {
        console.log(response);
        alert("Your details are updated!");
      },
      error =>
        alert(
          "Your details are not submitted. Please try again" + "error:" + error
        )
    );
  }

  recoverPassword() {
    this.authService.forgotPassword(this.email).subscribe(
      response => {
        console.log(response);
        alert("Email sent! Please check your email");
      },

      error => {
        console.log(error);
        alert("Enter a valid email");
      }
    );
  }


}