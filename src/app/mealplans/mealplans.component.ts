import { Component, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { Router } from '@angular/router'; 
import { authService } from '../../providers/auth.service';
import { ordersService } from '../../providers/order.service';
import { paymentService } from '../../providers/payment.service';
import { cartService } from '../../providers/cart.service';
import { Combos } from './mealplandata';
import { ModalDirective } from 'angular-bootstrap-md';

@Component({
  selector: 'app-mealplans',
  templateUrl: './mealplans.component.html',
  styleUrls: ['./mealplans.component.css']
})
export class MealPlans implements OnInit {
 
  products = Combos;
  menuItem: Subject<any> = new Subject;
  selectedProduct : Subject<any> = new Subject;
  addProduct : Subject<any> = new Subject;
  user: any = {};
  orders = [];
  clearForm: boolean;
  credentials: any;
  
public OrderSpecialty = {
  specialtyId:'',
  quantity:'',
  data: {
  price:'',
  name:'',
  image:'',
  description:'',
  title:'',
  } 
  }
 
  public processOrder = {
  name: '',
  sourceId:'',
  data:{
   firstname:'',
   lastname:'',
   street:'',
   city: '',
   state:'',
   zipcode:'',
   phone: '',
   email: '',
   deliveryDate: '',
   message: '',
  },
  specialties:[],
  stripeCustomerId: '' ,
  notes: '',

}
  
  constructor(private authService : authService, public router : Router, public paymentService : paymentService,
	    public orderService : ordersService, public cartService : cartService ) { 
   
   }

  ngOnInit() {
  this.authService.getMe().subscribe(res => {
            console.log(res);
			this.user = res;
		if(!this.user.stripeCustomerId){
        this.paymentService.createCustomer()
		.subscribe(data => { 
     console.log(data); 
		 
		});
      }  
	          this.processOrder.stripeCustomerId = this.user.stripeCustomerId;
			      this.processOrder.data.street = this.user.data.street ? this.user.data.street : '';
            this.processOrder.data.city = this.user.data.city ? this.user.data.city : '' ;
            this.processOrder.data.state = this.user.data.state ? this.user.data.state : '';
            this.processOrder.data.zipcode = this.user.data.zipcode ? this.user.data.zipcode : '';
			      this.processOrder.data.phone = this.user.data.phone ? this.user.data.phone : '';
            this.processOrder.data.firstname = this.user.firstName ? this.user.firstName: '' ;
            this.processOrder.data.lastname = this.user.lastName ? this.user.lastName: '' ;
            this.processOrder.data.email = this.user.email ? this.user.email: '' ;
			
      });

      this.orderService.getAllOrders()
      .subscribe(res => {
      this.orders = res;
      console.log(this.orders[0])
  })
}
  
  @ViewChild('signupModal') signupModal: ModalDirective;
  @ViewChild('createAccount') createAccount: ModalDirective;
  @ViewChild('activateAccount') activateAccount: ModalDirective;


  getdetails(details) {
    this.selectedProduct.next(details);
  }
  
  signup(details) {
    this.addProduct.next(details);
  }
 
  addToCart(details) {
  this.processOrder.specialties = details.product_combos.map(combo =>({
	specialtyId: combo.id,
	quantity: combo.quantity,
    data: {
    price: combo.price,
    name: combo.name,
    image: combo.image,
    description: combo.description,
    title: combo.title,
    }
	}));
	
   this.processOrder.name = "Meal Plan";
   console.log(this.processOrder)  
    if(this.authService.loggedIn()){
		 this.orderService.getAllOrders()
		 .subscribe(res => {
     this.orders = res;
     console.log(this.orders[0])
		  if( this.orders.length === 0 || this.orders[0].status !== 1){
		 	 this.orderService.addOrder(this.processOrder)
			 .subscribe(data => {
         console.log(data)
         this.router.navigate(['/shopping-cart']);
       }
         );	
			 console.log('add new')
		 } else {
		  console.log('modify')
		  const orderID = this.orders[0].specialties.map(order => order.specialtyId)
		  console.log(orderID)
		  const detailsID = details.product_combos.map(detail => detail.id)
		  console.log(detailsID)
          if( detailsID.toString() === orderID.toString())
           {  
		   for (var i = 0; i < this.orders[0].specialties.length; i++) {
           this.orders[0].specialties[i].quantity += 1;
		   }
		   console.log('added to existing order')
       console.log(this.orders[0])
      //  this.processOrder.specialties = this.orders[0].specialties;
      //  console.log(this.processOrder)
		    this.orderService.modifyOrder(this.orders[0], this.orders[0]._id)
			 .subscribe(data =>{ 
         console.log(data)
         this.orderService.getAllOrders()
         .subscribe(res => {
         this.orders = res;
         console.log(this.orders[0])
         this.router.navigate(['/shopping-cart']);
         })
        });	
           } else {
            details.product_combos.map(combo => {
              this.orders[0].specialties.push({
                specialtyId: combo.id,
                quantity: combo.quantity,
                 data: {
                 price: combo.price,
                 name: combo.name,
                 image: combo.image,
                 description: combo.description,
                 title: combo.title,
              }})
            })
        
			 console.log('added new combo')
       console.log(this.orders[0])
      //  this.processOrder.specialties = this.orders[0].specialties;
      //  this.processOrder._id = this.orders[0]._id;
      //  console.log(this.processOrder)
        this.orderService.modifyOrder(this.orders[0], this.orders[0]._id)
        .subscribe(data => {
          console.log(data)
          this.orderService.getAllOrders()
          .subscribe(res => {
          this.orders = res;
          console.log(this.orders[0])
          this.router.navigate(['/shopping-cart']);
      })
        }
          );	
		   }  
		  }
		 })
	   
	 } else {
     this.signupModal.show();
     this.signup(details) 
     this.clearForm = false;
   }  
}

myAccount(){
  this.menuItem.next('account');
  if(this.authService.loggedIn()){
    this.router.navigate(['/my-account']);
} else {
  this.signupModal.show();
  this.clearForm = false;
}
}

myOrder(event){
  if(event === 'account'){
  this.router.navigate(['/my-account']);
  } else {
  this.router.navigate(['/my-account/orders']);
  }
  this.closeModal();
  this.closeActivateModal();
}

closeModal(){
  this.signupModal.hide();
  this.clearForm = true;
}

closeCreateModal(){
  this.createAccount.hide();
  this.clearForm = true;
}

closeActivateModal(){
  this.activateAccount.hide();
}

resetPassword(){
  this.closeModal();
}

// myOrder(){
//   console.log(this.orders)
//   this.router.navigate(['/my-account/orders']);
//   this.closeModal();
// }

loginAccount(){
  this.closeCreateModal();
  this.closeActivateModal();
  this.signupModal.show();
}

create(){
  this.closeModal();
  this.createAccount.show();
}

activate(event){
  this.credentials = event;
  console.log(this.credentials)
  this.closeCreateModal();
  this.activateAccount.show();
}

}