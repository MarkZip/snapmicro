 export const Combos: any[] = [
      {
        id : "C1",
        img : "http://www.pachd.com/free-images/food-images/japanese-food-06.jpg",
		name : "Combo 1",
		price : 26.97,
		product_combos: [
		   {
		   title: "2-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1170",
		   price: 8.99,
		   quantity: 1, 
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Pinakbet",
		   description: "Vegetable stew cooked with ginger, garlic, onion, tomato, okra, string beans, squash, and eggplant"
		   },
		   {
		   title: "2-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1171",
		   price: 8.99,
		   quantity: 1, 
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Pork Adobo",
		   description: "Pork cooked in garlic, onions, bay leaves, soy sauce, and vinegar"
		   },
		     {
		   title: "2-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1172",
		   price: 8.99,
		   quantity: 1, 
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Chicken Kaldereta",
		   description: "Chicken cooked in garlic, onions, bay leaves, vinegar, tomato sauce, bell peppers, potatoes, and carrots"
		   }
		]
      },
	   {
        id : "C2",
        img : "http://www.pachd.com/free-images/food-images/japanese-food-06.jpg",
		name : "Combo 2", 
		price : 23.97, 
		product_combos: [
		   {
		   title: "2-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1173",
		   price: 7.99,
		   quantity: 1, 
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Tinola",
		   description: "Chicken cooked with ginger, garlic, onion, green papaya and spinach"
		   },
		   {
		   title: "2-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1174",
		   price: 7.99,
		   quantity: 1, 
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Lechon Kawali",
		   description: "Crispy roast pork"
		   },
		     { 
		   title: "2-Meal Combo Plan",
           id: "5bdbc0cb98ababd452be1175",
		   price: 7.99,
		   quantity: 1,
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Chopsuey",
		   description: "Vegetable saute with garlic, onions, bell peppers, baby corn, snap peas"
		   }
		]
	  },
	  {
        id : "C3",
        img : "http://www.pachd.com/free-images/food-images/japanese-food-06.jpg",
		name : "Combo 3", 
		price : 23.97, 
		product_combos: [
		   {
		   title: "2-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1176",
		   price: 7.99,
		   quantity: 1, 
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Chicken Inasal",
		   description: "Roast chicken marinated in lemon, orange, lime, pepper, coconut vinegar and annato"
		   },
		   {
		   title: "2-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1177",
		   price: 7.99,
		   quantity: 1, 
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Ginisang Ampalaya",
		   description: "Bittermelon sauteed in garlic, onion, tomatoes and eggs"
		   },
		     { 
		   title: "2-Meal Combo Plan",
           id: "5bdbc0cb98ababd452be1178",
		   price: 7.99,
		   quantity: 1,
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Pork Sinigang",
		   description: "Pork stew with onion, tomatoes, radish, long green beans, eggplant and okra"
		   }
		]
	  },
	  {
        id : "C4",
        img : "http://www.pachd.com/free-images/food-images/japanese-food-06.jpg",
		name : "Combo 4", 
		price : 23.97, 
		product_combos: [
		   {
		   title: "2-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1179",
		   price: 7.99,
		   quantity: 1, 
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Chicken Adobo",
		   description: "Chicken cooked in garlic, onions, bay leaves, soy sauce, and vinegar"
		   },
		   {
		   title: "2-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1180",
		   price: 7.99,
		   quantity: 1, 
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Pancit Bihon",
		   description: "Rice noodles cooked with soy sauce, garlic, onion, pork, chicken, snowpeas, carrots, cabbage, celery leaves"
		   },
		     { 
		   title: "2-Meal Combo Plan",
           id: "5bdbc0cb98ababd452be1181",
		   price: 7.99,
		   quantity: 1,
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Beef Pochero",
		   description: "Beef stew cooked with garlic, onion, tomato, cabbage, green beans, potato, garbanzos"
		   }
		]
	  },
	  {
        id : "C5",
        img : "http://www.pachd.com/free-images/food-images/japanese-food-06.jpg",
		name : "Combo 5", 
		price : 23.97, 
		product_combos: [
		   {
		   title: "2-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1182",
		   price: 7.99,
		   quantity: 1, 
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Sinampalukang Manok",
		   description: "Chicken cooked with ginger, garlic, onion, green papaya, tamarind leaves"
		   },
		   {
		   title: "2-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1183",
		   price: 7.99,
		   quantity: 1, 
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Pritong Bangus",
		   description: "Fried milkfish"
		   },
		     { 
		   title: "2-Meal Combo Plan",
           id: "5bdbc0cb98ababd452be1184",
		   price: 7.99,
		   quantity: 1,
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Ginisang Sayote",
		   description: "Chayote saute with garlic, onions, tomatoes and pork"
		   }
		]
	  },
	  {
        id : "C6",
        img : "http://www.pachd.com/free-images/food-images/japanese-food-06.jpg",
		name : "Combo 6", 
		price : 23.97, 
		product_combos: [
		   {
		   title: "2-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1185",
		   price: 7.99,
		   quantity: 1, 
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Pancit Palabok",
		   description: "Rice noodles cooked in pork broth and topped with pork, tofu, shrimp, fried pork skin, fried garlic, green onions and boiled eggs"
		   },
		   {
		   title: "2-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1186",
		   price: 7.99,
		   quantity: 1, 
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Lumpiang Shanghai",
		   description: "Spring rolls filled with a mix of ground pork and ground beef, carrots, onions"
		   },
		     { 
		   title: "2-Meal Combo Plan",
           id: "5bdbc0cb98ababd452be1187",
		   price: 7.99,
		   quantity: 1,
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Bichuelas",
		   description: "Beef stew with navy beans and cabbage"
		   }
		]
	  },
	  {
        id : "C7",
        img : "http://www.pachd.com/free-images/food-images/japanese-food-06.jpg",
		name : "Combo 7", 
		price : 39.95, 
		product_combos: [
		   {
		   title: "3-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1188",
		   price: 7.99,
		   quantity: 1, 
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "" ,
		   name: "Ginisang Munggo",
		   description: "Mung bean stew cooked with garlic, onion, tomato, spinach"
		   },
		   {
		   title: "3-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1189",
		   price: 7.99,
		   quantity: 1, 
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "1",
		   name: "Pork Adobo",
		   description: "Pork cooked in garlic, onions, bay leaves, soy sauce, and vinegar"
		   },
		     { 
		   title: "3-Meal Combo Plan",
           id: "5bdbc0cb98ababd452be1190",
		   price: 7.99,
		   quantity: 1,
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Ginisang Ampalaya",
		   description: "Bittermelon sauteed in garlic, onion, tomatoes and eggs"
		   },
		   { 
			title: "3-Meal Combo Plan",
			id: "5bdbc0cb98ababd452be1191",
			price: 7.99,
			quantity: 1,
			image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
			combo: "1",
			name: "Bistek Tagalog",
			description: "Beef sirloin sauteed in garlic, onions, soy sauce"
			},
			{ 
			 title: "3-Meal Combo Plan",
			 id: "5bdbc0cb98ababd452be1192",
			 price: 7.99,
			 quantity: 1,
			 image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
			 combo: "",
			 name: "Chicken Kaldereta",
			description: "Chicken stew cooked in garlic, onion, tomato, potatoes, carrots, bell peppers"
			}
		]
	},
			{
				id : "C8",
				img : "http://www.pachd.com/free-images/food-images/japanese-food-06.jpg",
				name : "Combo 8", 
				price : 39.95, 
				product_combos: [
				   {
				   title: "3-Meal Combo Plan",
				   id: "5bdbc0cb98ababd452be1193",
				   price: 7.99,
				   quantity: 1, 
				   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				   combo: "",
				   name: "Chicken Adobo",
				   description: "Chicken cooked in garlic, onions, bay leaves, soy sauce, and vinegar"
				   },
				   {
				   title: "3-Meal Combo Plan",
				   id: "5bdbc0cb98ababd452be1194",
				   price: 7.99,
				   quantity: 1, 
				   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				   combo: "1",
				   name: "Pinakbet",
				   description: "Vegetable stew cooked with ginger, garlic, onion, tomato, okra, string beans, squash, and eggplant"
				   },
					 { 
				   title: "3-Meal Combo Plan",
				   id: "5bdbc0cb98ababd452be1195",
				   price: 7.99,
				   quantity: 1,
				   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				   combo: "1",
				   name: "Beef Nilaga",
				   description: "Beef stew with bok choy, carrots, and potatoes"
				   },
				   { 
					title: "3-Meal Combo Plan",
					id: "5bdbc0cb98ababd452be1196",
					price: 7.99,
					quantity: 1,
					image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
					combo: "",
					name: "Pancit Bihon",
					description: "Rice noodles cooked with soy sauce, garlic, onion, pork, chicken, snowpeas, carrots, cabbage, celery leaves"
					},
					{ 
					 title: "3-Meal Combo Plan",
					 id: "5bdbc0cb98ababd452be1197",
					 price: 7.99,
					 quantity: 1,
					 image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
					 combo: "",
					 name: "Lumpiang Shanghai",
					description: "Spring rolls filled with a mix of ground pork and ground beef, carrots, onions"
					},
				]
		   
	  },
	  {
		id : "C9",
		img : "http://www.pachd.com/free-images/food-images/japanese-food-06.jpg",
		name : "Combo 9", 
		price : 31.96, 
		product_combos: [
		   {
		   title: "3-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1198",
		   price: 7.99,
		   quantity: 1, 
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "1",
		   name: "Beef Pochero",
		   description: "Beef stew cooked with garlic, onion, tomato, cabbage, green beans, potato, garbanzos"
		   },
		   {
		   title: "3-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1199",
		   price: 7.99,
		   quantity: 1, 
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "",
		   name: "Lechon Kawali",
		   description: "Crispy roast pork"
		   },
			 { 
		   title: "3-Meal Combo Plan",
		   id: "5bdbc0cb98ababd452be1200",
		   price: 7.99,
		   quantity: 1,
		   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		   combo: "1",
		   name: "Pancit Palabok",
		   description: "Rice noodles cooked in pork broth and topped with pork, tofu, shrimp, fried pork skin, fried garlic, green onions and boiled eggs"
		   },
		   { 
			title: "3-Meal Combo Plan",
			id: "5bdbc0cb98ababd452be1201",
			price: 7.99,
			quantity: 1,
			image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
			combo: "",
			name: "Tinola",
			description: "Chicken cooked with ginger, garlic, onion, green papaya and spinach"
			},
		]
   
},
{
	id : "C10",
	img : "http://www.pachd.com/free-images/food-images/japanese-food-06.jpg",
	name : "Combo 10", 
	price : 31.96, 
	product_combos: [
	   {
	   title: "3-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1202",
	   price: 7.99,
	   quantity: 1, 
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "",
	   name: "Beef Tapa",
	   description: "Chicken cooked with ginger, garlic, onion, green papaya and spinach"
	   },
	   {
	   title: "3-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1203",
	   price: 7.99,
	   quantity: 1, 
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "1",
	   name: "Chopsuey",
	   description: "Vegetable saute with garlic, onions, bell peppers, baby corn, snap peas"
	   },
		 { 
	   title: "3-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1204",
	   price: 7.99,
	   quantity: 1,
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "1",
	   name: "Pork Sinigang",
	   description: "Pork stew with onion, tomatoes, radish, long green beans, eggplant and okra"
	   },
	   { 
		title: "3-Meal Combo Plan",
		id: "5bdbc0cb98ababd452be1205",
		price: 7.99,
		quantity: 1,
		image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		combo: "",
		name: "Chicken Kaldereta",
		description: "Chicken stew cooked in garlic, onion, tomato, potatoes, carrots, bell peppers"
		},
	]

},
{
	id : "C11",
	img : "http://www.pachd.com/free-images/food-images/japanese-food-06.jpg",
	name : "Combo 11", 
	price : 31.96, 
	product_combos: [
	   {
	   title: "3-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1206",
	   price: 7.99,
	   quantity: 1, 
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "",
	   name: "Pritong Bangus",
	   description: "fried milkfish"
	   },
	   {
	   title: "3-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1207",
	   price: 7.99,
	   quantity: 1, 
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "1",
	   name: "Misua",
	   description: "Japanese somen noodle soup with ground pork"
	   },
		 { 
	   title: "3-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1208",
	   price: 7.99,
	   quantity: 1,
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "1",
	   name: "Bicol Express",
	   description: "Pork cooked with garlic, onion, green chili peppers, shrimp paste and coconut cream"
	   },
	   { 
		title: "3-Meal Combo Plan",
		id: "5bdbc0cb98ababd452be1209",
		price: 7.99,
		quantity: 1,
		image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		combo: "",
		name: "Sinampalukang Manok",
		description: "Chicken cooked with ginger, garlic, onion, green papaya, tamarind leaves"
		},
	]

},
{
	id : "C12",
	img : "http://www.pachd.com/free-images/food-images/japanese-food-06.jpg",
	name : "Combo 12", 
	price : 39.95, 
	product_combos: [
	   {
	   title: "3-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1210",
	   price: 7.99,
	   quantity: 1, 
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "1",
	   name: "Bichuelas",
	   description: "Beef stew with navy beans and cabbage"
	   },
	   {
	   title: "3-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1211",
	   price: 7.99,
	   quantity: 1, 
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "",
	   name: "Chicken Inasal",
	   description: "Roast chicken marinated in lemon, orange, lime, pepper, coconut vinegar and annato"
	   },
		 { 
	   title: "3-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1212",
	   price: 7.99,
	   quantity: 1,
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "1",
	   name: "Ginisang Sayote",
	   description: "Chayote saute with garlic, onions, tomatoes and pork"
	   },
	   { 
		title: "3-Meal Combo Plan",
		id: "5bdbc0cb98ababd452be1213",
		price: 7.99,
		quantity: 1,
		image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		combo: "",
		name: "Pancit Bihon",
		description: "Rice noodles cooked with soy sauce, garlic, onion, pork, chicken, snowpeas, carrots, cabbage, celery leaves"
		},
		{ 
		 title: "3-Meal Combo Plan",
		 id: "5bdbc0cb98ababd452be1214",
		 price: 7.99,
		 quantity: 1,
		 image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		 combo: "",
		 name: "Beef Nilaga",
		description: "Beef shank stew with potatoes, cabbage, green beans"
		},
	]

},
{
	id : "C13",
	img : "http://www.pachd.com/free-images/food-images/japanese-food-06.jpg",
	name : "Combo 13", 
	price : 71.91, 
	product_combos: [
	   {
	   title: "5-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1215",
	   price: 7.99,
	   quantity: 1, 
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "",
	   name: "Beef Tapa",
	   description: "Beef sirloin fry marinated with chopped garlic, soy sauce, vinegar, sugar"
	   },
	   {
	   title: "5-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1216",
	   price: 7.99,
	   quantity: 1, 
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "1",
	   name: "Pinakbet",
	   description: "Vegetable stew cooked with ginger, garlic, onion, tomato, okra, string beans, squash, and eggplant"
	   },
		 { 
	   title: "5-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1217",
	   price: 7.99,
	   quantity: 1,
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "1",
	   name: "Sinampalukang Manok",
	   description: "Chicken cooked with ginger, garlic, onion, green papaya, tamarind leaves"
	   },
	   { 
		title: "5-Meal Combo Plan",
		id: "5bdbc0cb98ababd452be1218",
		price: 7.99,
		quantity: 1,
		image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		combo: "",
		name: "Pritong Bangus",
		description: "fried milkfish"
		},
		{ 
			title: "5-Meal Combo Plan",
			id: "5bdbc0cb98ababd452be1219",
			price: 7.99,
			quantity: 1,
			image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
			combo: "1",
			name: "Bicol Express",
			description: "Pork cooked with garlic, onion, green chili peppers, shrimp paste and coconut cream"
			},
			{ 
			 title: "5-Meal Combo Plan",
			 id: "5bdbc0cb98ababd452be1210",
			 price: 7.99,
			 quantity: 1,
			 image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
			 combo: "",
			 name: "Pancit Palabok",
			 description: "Rice noodles cooked in pork broth and topped with pork, tofu, shrimp, fried pork skin, fried garlic, green onions and boiled eggs"
			 },
			 { 
				title: "5-Meal Combo Plan",
				id: "5bdbc0cb98ababd452be1211",
				price: 7.99,
				quantity: 1,
				image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "1",
				name: "Lumpia",
				description: "Spring rolls filled with a mix of ground pork and ground beef, carrots, onions"
				},
				{ 
				 title: "5-Meal Combo Plan",
				 id: "5bdbc0cb98ababd452be1212",
				 price: 7.99,
				 quantity: 1,
				 image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				 combo: "",
				 name: "Ginisang Ampalaya",
				 description: "Bittermelon sauteed in garlic, onion, tomatoes and eggs"
				 },
				 { 
					title: "5-Meal Combo Plan",
					id: "5bdbc0cb98ababd452be1213",
					price: 7.99,
					quantity: 1,
					image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
					combo: "",
					name: "Pork Adobo",
					description: "Pork cooked in garlic, onions, bay leaves, soy sauce, and vinegar"
					},
	]

},
{
	id : "C14",
	img : "http://www.pachd.com/free-images/food-images/japanese-food-06.jpg",
	name : "Combo 14", 
	price : 55.93, 
	product_combos: [
	   {
	   title: "5-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1214",
	   price: 7.99,
	   quantity: 1, 
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "1",
	   name: "Beef Nilaga",
	   description: "Beef shank stew with potatoes, cabbage, green beans"
	   },
	   {
	   title: "5-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1215",
	   price: 7.99,
	   quantity: 1, 
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "",
	   name: "Lechon Kawali",
	   description: "Crispy roast pork"
	   },
		 { 
	   title: "5-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1216",
	   price: 7.99,
	   quantity: 1,
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "1",
	   name: "Ginisang Munggo",
	   description: "Mung bean stew cooked with garlic, onion, tomato, spinach"
	   },
	   { 
		title: "5-Meal Combo Plan",
		id: "5bdbc0cb98ababd452be1217",
		price: 7.99,
		quantity: 1,
		image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		combo: "",
		name: "Chopsuey",
		description: "Vegetable saute with garlic, onions, bell peppers, baby corn, snap peas"
		},
		{ 
			title: "5-Meal Combo Plan",
			id: "5bdbc0cb98ababd452be1218",
			price: 7.99,
			quantity: 1,
			image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
			combo: "1",
			name: "Chicken Adobo",
			description: "Chicken cooked in garlic, onions, bay leaves, soy sauce, and vinegar"
			},
			{ 
			 title: "5-Meal Combo Plan",
			 id: "5bdbc0cb98ababd452be1219",
			 price: 7.99,
			 quantity: 1,
			 image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
			 combo: "1",
			 name: "Beef Pochero",
			 description: "Beef stew cooked with garlic, onion, tomato, cabbage, green beans, potato, garbanzos"
			 },
			 { 
				title: "5-Meal Combo Plan",
				id: "5bdbc0cb98ababd452be1220",
				price: 7.99,
				quantity: 1,
				image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "",
				name: "Tinola",
				description: "Chicken cooked with ginger, garlic, onion, green papaya and spinach"
				},
	]

},
{
	id : "C15",
	img : "http://www.pachd.com/free-images/food-images/japanese-food-06.jpg",
	name : "Combo 15", 
	price : 63.92, 
	product_combos: [
	   {
	   title: "5-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1221",
	   price: 7.99,
	   quantity: 1, 
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "",
	   name: "Chicken Inasal",
	   description: "Roast chicken marinated in lemon, orange, lime, pepper, coconut vinegar and annato"
	   },
	   {
	   title: "5-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1222",
	   price: 7.99,
	   quantity: 1, 
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "1",
	   name: "Misua",
	   description:  "Japanese somen noodle soup with ground pork"
	   },
		 { 
	   title: "5-Meal Combo Plan",
	   id: "5bdbc0cb98ababd452be1223",
	   price: 7.99,
	   quantity: 1,
	   image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
	   combo: "1",
	   name: "Bichuelas",
	   description: "Beef stew with navy beans and cabbage"
	   },
	   { 
		title: "5-Meal Combo Plan",
		id: "5bdbc0cb98ababd452be1224",
		price: 7.99,
		quantity: 1,
		image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
		combo: "",
		name: "Pancit Bihon",
		description: "Rice noodles cooked with soy sauce, garlic, onion, pork, chicken, snowpeas, carrots, cabbage, celery leaves"
		},
		{ 
			title: "5-Meal Combo Plan",
			id: "5bdbc0cb98ababd452be1225",
			price: 7.99,
			quantity: 1,
			image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
			combo: "1",
			name: "Chicken Kaldereta",
			description: "Chicken stew cooked in garlic, onion, tomato, potatoes, carrots, bell peppers"
			},
			{ 
			 title: "5-Meal Combo Plan",
			 id: "5bdbc0cb98ababd452be1226",
			 price: 7.99,
			 quantity: 1,
			 image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
			 combo: "",
			 name: "Bistek Tagalog",
			 description: "Beef sirloin sauteed in garlic, onions, soy sauce"
			 },
			 { 
				title: "5-Meal Combo Plan",
				id: "5bdbc0cb98ababd452be1227",
				price: 7.99,
				quantity: 1,
				image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
				combo: "1",
				name: "Ginisang Sayote",
				description: "Chayote saute with garlic, onions, tomatoes and pork"
				},
				{ 
					title: "5-Meal Combo Plan",
					id: "5bdbc0cb98ababd452be1228",
					price: 7.99,
					quantity: 1,
					image: "http://www.pachd.com/free-images/food-images/japanese-food-04.jpg",
					combo: "",
					name: "Pork Sinigang",
					description: "Pork stew with onion, tomatoes, radish, long green beans, eggplant and okra"
					},
	]

},
	]
