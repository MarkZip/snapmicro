import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { AppConfig } from './app.config';
//import { Router } from '@angular/router';
import { authService } from './auth.service';

@Injectable()

export class ordersService {
  constructor(
    private _http: Http, 
   // private _router: Router,
    private _authService: authService
  ) {}

  public addOrder(orderSpecialty) {
  //order with orderspecialty inside specialties
  //will go to checkout page / buy it now page 
    const body = orderSpecialty;
    const token = this._authService.getAccessToken();
    const options = new RequestOptions({
      headers: new Headers({'Authorization': token})
    });
    return this._http.post(`${AppConfig.baseUrl}/users/me/orders`, body, options)
      .map((res) => {
        const response = res.json();
        return response;
      });
  }

  public getOrder(orderID) {
    const token = this._authService.getAccessToken();
    const options = new RequestOptions({
      headers: new Headers({'Authorization': token})
    });
    return this._http.get(`${AppConfig.baseUrl}/users/me/orders/${orderID}`, options)
      .map((res) => {
        const response = res.json();
        return response;
      });
  }

  public getAllOrders() {
    const token = this._authService.getAccessToken();
    const options = new RequestOptions({
      headers: new Headers({'Authorization': token})
    });
    return this._http.get(`${AppConfig.baseUrl}/users/me/orders`, options)
      .map((res) => {
        const response = res.json();
        return response;
      });
  }

  public getAllVendorOrders() {
  //for vendors only
    const token = this._authService.getAccessToken();
    const options = new RequestOptions({
      headers: new Headers({'Authorization': token})
    });
    return this._http.get(`${AppConfig.baseUrl}/users/me/vendor/orders`, options)
      .map((res) => {
        const response = res.json();
        return response;
      });
  }
  

  public modifyOrderVendors(order, orderid:string) {
  //(before payment) rejected / accepted / (after paid) cancel or refund buyer
    const body = order;
    const token = this._authService.getAccessToken();
    const options = new RequestOptions({
      headers: new Headers({'Authorization': token})
    });
    return this._http.put(`${AppConfig.baseUrl}/users/me/vendor/orders/${orderid}`, body, options)
      .map((res) => {
        const response = res.json();
        return response;
      });
  }

  public modifyOrder(order, orderid:string) {
  //customer still unpaid changed mind
    const body = order;
    const token = this._authService.getAccessToken();
    const options = new RequestOptions({
      headers: new Headers({'Authorization': token})
    });
    return this._http.put(`${AppConfig.baseUrl}/users/me/orders/${orderid}`, body, options)
      .map((res) => {
        const response = res.json();
        return response;
      });
  }

  public chargeVendor(orderid:string) {
    const body = {};
    const token = this._authService.getAccessToken();
    const options = new RequestOptions({
      headers: new Headers({'Authorization': token})
    });
    return this._http.post(`${AppConfig.baseUrl}/users/me/vendor/orders/${orderid}/charge`, body, options)
      .map((res) => {
        const response = res.json();
        return response;
      });
  }

  public cancelPaidOrder(orderid:string) {
  //customer if paid
    const body = {};
    const token = this._authService.getAccessToken();
    const options = new RequestOptions({
      headers: new Headers({'Authorization': token})
    });
    return this._http.put(`${AppConfig.baseUrl}/users/me/orders/${orderid}/cancel`, body, options)
      .map((res) => {
        const response = res.json();
        return response;
      });
  }

  public cancelPaidOrderVendors(orderid:string) {
  //vendor if paid
    const body = {};
    const token = this._authService.getAccessToken();
    const options = new RequestOptions({
      headers: new Headers({'Authorization': token})
    });
    return this._http.put(`${AppConfig.baseUrl}/users/me/vendor/orders/${orderid}/cancel`, body, options)
      .map((res) => {
        const response = res.json();
        return response;
      });
  }

  public deleteOrder(orderID:string) {
  //only if cancelPaidOrderVendor is called
    const token = this._authService.getAccessToken();
    const options = new RequestOptions({
      headers: new Headers({'Authorization': token})
    });
    return this._http.delete(`${AppConfig.baseUrl}/users/me/orders/${orderID}`, options)
      .map((res) => {
        const response = res.json();
        return response;
      });
  }
}