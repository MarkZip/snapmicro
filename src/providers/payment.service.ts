import { Injectable } from '@angular/core';
import { AppConfig } from './app.config';
import { Http, Headers, Response, RequestOptions} from '@angular/http';
import { Observable } from 'rxjs';
import { authService } from './auth.service';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/toPromise';

@Injectable()
export class paymentService {

  constructor(
    private http: Http,
    private _authService: authService
  ) {}

  public getOptions():RequestOptions {
    const token = this._authService.getAccessToken();
    return new RequestOptions({
      headers: new Headers({
        'Authorization': token,
        'Content-Type': 'application/json'
      })
    });
  }

  public createCustomer(): Observable<any> {
    return this.http.post(`${AppConfig.baseUrl}/users/me/paymentAccount`, {}, this.getOptions())
      .map((response: Response) => response.json().data)
  };

  public addCard(source: String): Observable<any> {
    return this.http.post(`${AppConfig.baseUrl}/users/me/cards`, {source: source}, this.getOptions())
      .map((response: Response) => response.json().data)
  };

  public createStripeAccount(id: String, code: String): Observable<any> {
    return this.http.get(`${AppConfig.baseUrl}/users/${id}/stripeConnect/${code}`, this.getOptions())
      .map((response: Response) => response.json().data)
  };

  public addSubscription(amount: number, name: String, orderId: String): Observable<any> {
    return this.http.post(`${AppConfig.baseUrl}/users/me/subscriptions`, {amount: amount, name: name, orderId: orderId}, this.getOptions())
      .map((response: Response) => response.json().data)
  };

  public addBankAccount(publicToken: String, accountId: String): Observable<any> {
    return this.http.post(`${AppConfig.baseUrl}/users/me/bankAccounts`, {publicToken: publicToken, accountId: accountId}, this.getOptions())
      .map((response: Response) => response.json())
  };

  public getCards(): Observable<any> {
    return this.http.get(`${AppConfig.baseUrl}/users/me/cards`, this.getOptions())
      .map((response: Response) => response.json().data)
  };

  public getBankAccounts(): Observable<any> {
    return this.http.get(`${AppConfig.baseUrl}/users/me/bankAccounts`, this.getOptions())
      .map((response: Response) => response.json().data)
  };

  public deleteCard(token: String): Observable<any> {
    return this.http.delete(`${AppConfig.baseUrl}/users/me/cards/${token}`, this.getOptions())
      .map((response: Response) => response.json().data)
  };

  public deleteBankAccount(token: String): Observable<any> {
    return this.http.delete(`${AppConfig.baseUrl}/users/me/bankAccounts/${token}`, this.getOptions())
      .map((response: Response) => response.json().data)
  };
}