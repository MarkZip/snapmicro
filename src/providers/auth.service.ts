import { Injectable, ViewChild } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';
import { AppConfig } from './app.config';
import { Router } from '@angular/router';

@Injectable()
export class authService {

  constructor(private _http: Http) {}
  

  public getOptions():RequestOptions {
    const token = this.getAccessToken();
    return new RequestOptions({
      headers: new Headers({'Authorization': token})
    });
  }

  public login(email: string, password: string): Observable<Response> {
    const data = {
      email: email,
      password: password
    };
    return this._http.post(`${AppConfig.baseUrl}/auth/local`, data)
      .map((res) => {
        const response = res.json();
        localStorage.setItem('accessToken', response.token);
        localStorage.setItem('myUserId', response.userId);
        return response;
      });
  }

  public signup(email: string, password: string, firstName: string, lastName:string): Observable<Response> {
    const data = {
      email,
      password,
      firstName,
      //lastName 
    };
    return this._http.post(`${AppConfig.baseUrl}/users`, data)
      .map((res) => {
        localStorage.setItem('myUsername', name);
        return res.json();
      });
  }

  public logout() {
   // this._router.navigate(['']);
    localStorage.clear();
  }

  public loggedIn() {
    const token = localStorage.getItem('accessToken');
    if (!!token && token != 'undefined') {
      return true;
    }
    return false;
  }

  public getAccessToken() {
    const token = localStorage.getItem('accessToken');
    if (token) {
      return 'Bearer ' + token;
    }
    return token;
  }

  public activate(activateToken) {
    const data = {
      activateToken: activateToken
    };
    return this._http.put(`${AppConfig.baseUrl}/users/activation`, data)
      .map((res) => {
        return res;
      });
  }

  public reactivate() {
    const token = this.getAccessToken();
    const headers = new Headers({'Authorization': token});
    return this._http.put(`${AppConfig.baseUrl}/users/reactivation`, headers)
      .map((res) => {
        return res.json();
      });
  }

  public forgotPassword(email) {
    const data = {
      email: email
    };
    return this._http.post(`${AppConfig.baseUrl}/users/forgotpassword`, data)
      .map((res) => {
        return res.json();
      });
  }

  public changePassword(resetToken, newPassword) {
    const body = {
      resetToken: resetToken,
      newPassword: newPassword
    };
    return this._http.put(`${AppConfig.baseUrl}/users/me/password`, body, this.getOptions())
      .map((res) => {
        return res;
      });
  }

  public changeEmail(newEmail) {
    const body = newEmail;
    return this._http.put(`${AppConfig.baseUrl}/users/me/email`, body, this.getOptions())
      .map((res) => {
        return res.json();
      });
  }

  public updateMe(obj) {
    // 0 =  unverified, 1=verified user, 2=unverified vendor, 3=verified user
    const body = obj;
    return this._http.put(`${AppConfig.baseUrl}/users/me`, body, this.getOptions())
      .map((res) => {
        return res.json();
      });
  }
  
  public getMe() {
    if (!this.loggedIn) return;
    return this._http.get(`${AppConfig.baseUrl}/users/me`, this.getOptions())
      .map((res) => {
        let data = res.json();
        data.firstName = this.capitalize(data, 'firstName');
        data.lastName = this.capitalize(data, 'lastName');
        return data;
      });
  }

  public getUser(id) {
    return this._http.get(`${AppConfig.baseUrl}/users/${id}`, this.getOptions())
      .map((res) => {
        let data = res.json();
        data.firstName = this.capitalize(data, 'firstName');
        data.lastName = this.capitalize(data, 'lastName');
        return data;
      });
  }

  private capitalize(obj, key) {
    if (obj[key]) {
      return obj[key].charAt(0).toUpperCase() + obj[key].slice(1).toLowerCase();
    }
  }

  loginFacebook(cb) {
    const self = this;
    const token = this.getAccessToken();
    let res;

    (<any>window).addEventListener("message", (event) => {
      if(event && event.data && event.data.token && event.data.userId){
        localStorage.setItem('accessToken', event.data.token);
        localStorage.setItem('myUserId', event.data.userId);
      }
	  cb();
      _window.close();
    }, false);

    let _window = (<any>window).open(AppConfig.baseUrl + '/auth/facebook', 'Facebook Login', 'width=500, height=600');
  }

  loginGoogle(cb) {
    const self = this;
    const token = this.getAccessToken();
    let res;
    
    (<any>window).addEventListener("message", (event) => {
      if(event && event.data && event.data.token && event.data.userId){
        localStorage.setItem('accessToken', event.data.token);
        localStorage.setItem('myUserId', event.data.userId);
      }
	   cb();
      _window.close();
    }, false);

    let _window = (<any>window).open(AppConfig.baseUrl + '/auth/google', 'Google Login', 'width=500, height=600');
  }
}